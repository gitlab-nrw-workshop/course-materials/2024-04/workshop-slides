"""This script creates an issue template based on a list of projects
and a jinja template. This new issue template can then be committed by Stuart.
"""
import click
from jinja2 import Environment, FileSystemLoader
import yaml
import os


@click.command()
@click.option(
    "--template",
    "-t",
    type=click.Path(exists=True),
    help="Path to the target jinja template file.",
)
@click.option(
    "--metadata",
    "-m",
    type=click.Path(exists=True),
    help="Path to a YAML file containing workshop metadata.",
)
@click.option(
    "--outfile",
    "-o",
    type=click.Path(),
    default="slides.md",
    help="Output path for filled file",
)
def fill_template(template, metadata, outfile):
    """Use jinja to fill the template."""
    # Set up templating environment
    template_folder = os.path.dirname(template)
    environment = Environment(loader=FileSystemLoader(template_folder))
    chapter_env = Environment(loader=FileSystemLoader("chapters/"))

    # Read names from yaml file
    with open(metadata, "r") as metadata_file:
        workshop_metadata = yaml.safe_load(metadata_file.read())

    # Render chapter templates using the variables defined above
    rendered_chapters = []
    for chapter_template in chapter_env.list_templates():
        chapter_tmplt = chapter_env.get_template(chapter_template)
        rendered_chapters.append(chapter_tmplt.render(workshop=workshop_metadata))

    # Fill out template file using the yaml content
    main_template = os.path.basename(template)
    tmplt = environment.get_template(main_template)

    rendered_template = tmplt.render(
        chapters=rendered_chapters, workshop=workshop_metadata
    )

    with open(outfile, "w") as slides_file:
        slides_file.write(rendered_template)


if __name__ == "__main__":
    fill_template()
