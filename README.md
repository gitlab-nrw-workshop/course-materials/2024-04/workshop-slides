# Einstieg ins Forschungsdatenmanagement mit Git und GitLab

Datum: 16.04.2024, 09:00 - 14:00 Uhr

Format: Flipped Classroom in und mit GitLab, ~1,5h Vorbereitungszeit

## Folien

<a href="https://gitlab-nrw-workshop.gitlab.io/course-materials/2024-04/workshop-slides/slides.html">Vortragsfolien</a>
