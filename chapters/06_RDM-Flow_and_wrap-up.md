
---

# Exercise: Automation with GitLab — RDM Flow

We assigned you a project named [`project-<NR>`]({{ workshop.links.rdm_flow_group}}) for this task. Please use this project 

* We will show, how to set up a GitLab Project for automatic data quality control
* Please code along in your assigned project
* Please ask, if there are questions!

---
class: title-green

# GitLab Examples

## Collection of common RDM tasks solved with GitLab
https://gitlab.com/fdm-nrw-gitlabexamples/index

## Example for automatic data visualization
https://gitlab.com/fdm-nrw-gitlabexamples/pythondatavisualization


---
class: title-blue

# Wrap Up

## Git
- Decentral versioning of text files
- Commits → states of files
- Branches for concurrent work

## GitLab
- Manages projects that contain git repositories
- Project management features: Issues, MRs
- Automation features: Pipelines


## Questions, Feedback?
[Concept Board]({{ workshop.links.concept_board_feedback }})
