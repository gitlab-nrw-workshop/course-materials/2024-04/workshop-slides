
---
class: title-green

# Exercise: Git Tac Toe

---
class: img-right

# Git Tac Toe - Rules

![Tic Tac Toe](img/tictactoe.svg)

* 2 Players: ❌ and ⭕
* Played on a 3x3 grid
* In their turn, a player places their symbol in one slot in the grid
* The players who first fills a column, row, or diagonal with their symbol wins

In a text file, the grid looks like this:

```python
 | | 
-+-+-
 | | 
-+-+-
 | | 
```

---
class: dense


# Git Tac Toe (Round 1) - *Taking Turns*

Preparation: Groups with two players ❌ und ⭕

> ❌ creates a new GitLab project and initializes it with a README

> ❌ adds ⭕ to the project as a *Maintainer* 

Preparation: Both players clone the repository to their computer

Preparation: ❌ creates a new file `round_1.txt` and fills it with the tic-tac-toe grid.

```bash
X> git add round_1.txt
X> git commit -m "Grid for round 1"
```

---
class: dense
name: task1

# Git Tac Toe (Round 1) - *Taking Turns*

Turn: ❌ takes the first turn
```bash
X> git add round_1.txt
X> git commit -m "X turn 1"
X> git push
```

Turn: ⭕ updates their local repository and takes their turn

```bash
O> git pull
```

> ⭕ adds their turn to the grid

```bash
O> git add round_1.txt
O> git commit -m "O turn 1"
O> git push
```
Turn: ❌ updates their repository and takes their turn.

... and so on ...

---
class: wide

# Git Tac Toe (Round 1) - *Your Turn(s)* (10 min)


.center[
![](img/git_via_menu.gif)
]


---
class: sparse

# Git Tac Toe (Round 1) - *Your Turn(s)* (10 min)

- We will sort you into breakout rooms
- Decide who is player ❌ and start playing
  - [Git cheat sheet](https://raw.githubusercontent.com/hbons/git-cheat-sheet/master/preview.png)
  - [Link to slides]({{ workshop.links.slides }})
- Feel free to share your screens

If there are any problems, please feel free to ask!

---

# Git Tac Toe (Round 2) - *Conflicting Turns*

Preparation: Player ❌ creates a new file `round_2.txt` and fills it with the tic-tac-toe grid.

```bash
X> git add round_2.txt
X> git commit -m "Grid for GitTacToe"
X> git push
```

Preparation: Both players pull the changes from the repository.

```bash
X&O> git pull
```

Turns: both ❌ and ⭕ take their turns and commit them

```bash
X&O> git add round_2.txt
X&O> git commit –m "{X,O} turn 1"
```

---
class: dense

# Git Tac Toe (Round 2) - *Conflicting Turns*

Turns: Player ⭕ starts to send their turn to the server

```bash
X&O> git push
```
> *The push of player ❌ will be rejected!*

❌ needs to resolve the conflict:

```bash
X> git pull
```

* Either: git can automatically resolve the merge
* Or: ❌ edits `round_2.txt` and merges the game states manually.

```bash
X> git add round_2.txt
X> git commit -m "Turn 1 merged"
X> git push
```
... and so on ... 

---
class: sparse

# Git Tac Toe (Round 2) - *Your Turn(s)* (10 min)

- We will sort you into breakout rooms
- Feel free to share your screens

If there are any problems, please feel free to ask!

---
class: sparse

# Git Tac Toe (Round 2) - *Wrap Up*

- How resolving conflicts is supposed to look
- How to prevent this problem?
  - Split files
  - Branches
- Questions?

---
class: sparse

# Tools for working with git
* GitLab et al. allow collaborative work
  * Help for merges (*Merge Requests*)<br>Example:
  ```
  https://git.rwth-aachen.de/coscine/graphs/applicationprofiles/-/merge_requests/115
  ```
  * Can employ automated tests
  * More on this in Block 3
* Git GUIs


---
class: title-blue, sparse

# Wrap Up

* Basics of Git
* Synchronizing file version histories between a local and a remote repository
* Commands: `git init   clone   add   commit   push   pull`
* Git Tac Toe
* Resolving conflicts

