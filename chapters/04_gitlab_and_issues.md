
---
class: title-green

# GitLab Walkthrough

---

# GitLab Walkthrough: *Wiki*

## Documentation of the project 
* Are themselves git repositories
* Pages can be linked
* Formatted via Markdown
* Images, videos and source code can be displayed

---

# GitLab Walkthrough: *Issues*

## Project management in the repository
* Issues define work packages
* Can be assigned to users
* Can be grouped in lists and milestones
* Several views onto the same issue are possible
* Can refer directly to code changes (Merge Requests)


---

# GitLab Walkthrough: *CI/CD*

## Execution of workflows on files in the repository
* Workflow is defined in the repository in the file `.gitlab-ci.yml`
* Can automatically be executed on changes to the repository
* Can be executed in regular intervals ("cron")
* Allows automation of e.g. tests or publication

---
class: title-blue, img-only

# *Lunch Break (45 min)*

![](img/pause1.jpg)

Foto: Marco Verch | [ccnull.de](https://ccnull.de/foto/coffe-break-concept-with-cup-of-coffee-and-mobile-phone-on-the-table/1013743) | CC-BY 2.0


---
class: title-green

# Exercise: Working with Issues

---
class: sparse

# Exercise: Working with Issues (5 min)

We invited you to a GitLab project. In this project:

* Create some new issues:
  * Write a short description text
  * Experiment with markdown formatting
  * Assign these to a user, e.g. yourself
* Explore the options to assign deadlines, labels, etc.
* Close issues assigned to you
