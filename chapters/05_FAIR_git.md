
---
class: title-green

# FAIR Principles with Git


---
class: dense

# Recap FAIR Principles *(1)*
## To be Findable:

* F1. (Meta)data are assigned a globally unique and persistent identifier
* F2. Data are described with rich metadata
* F3. Metadata clearly and explicitly include the identifier of the data they describe
* F4. (Meta)data are registered or indexed in a searchable resource

--

## To be Accessible:

* A1. (Meta)data are retrievable by their identifier using a standardised communications protocol
  * A1.1 The protocol is open, free, and universally implementable
  * A1.2 The protocol allows for an authentication and authorisation procedure, where necessary
* A2. Metadata are accessible, even when the data are no longer available

---
class: dense

# Recap FAIR Principles *(2)*

## To be Interoperable:

* I1. (Meta)data use a formal, accessible, shared, and broadly applicable language for knowledge representation.
* I2. (Meta)data use vocabularies that follow FAIR principles
* I3. (Meta)data include qualified references to other (meta)data

--

## To be Re-usable:

* R1. (Meta)data are richly described with a plurality of accurate and relevant attributes
  * R1.1. (Meta)data are released with a clear and accessible data usage license
  * R1.2. (Meta)data are associated with detailed provenance
  * R1.3. (Meta)data meet domain-relevant community standards

---
class: dense

# How are FAIR Principles addressed by Git and GitLab? *(1)*

## Findable

* Each project recieves a unique URL (F1)
* All versions of all files can be found in `git log` via their commit hashes (F3)
* File contents and metadata can be retrieved using commit hashes (F1)
* GitLab indexes projects and makes them discoverable through search engines (F4)

--

## Accessible

* Files can be retrieved via SSH and HTTPS (A1.1)
* Both protocols provide authentication procedures (A1.2)

---
class: dense

# How are FAIR Principles addressed by Git and GitLab? *(2)*

## Interoperable
* Special files (e.g. `Readme.md`, `LICENSE`) are detected and converted to structured metadata (I3)
* GitLab projects use open, text-based file formats (Plain text, Markdown, YAML) (I1)
* Automatic handling of specific metadata formats (e.g. YAML-Frontmatter) (I2)

--

## Reusable

* Versioning enables provenance tracking of data (R1.2)
* License information is explicitly stated in GitLab's UI (R1.1)

---
class: sparse

# FAIR Principles in git and GitLab - What is missing? 

--

* Discipline-specific "rich" metadata (F2, I2, R1.3)
  * GitLab metadata is aimed towards software development
  * Available metadata fields are generic: Licensen, Authors, Versions
* Scientific documentation standards (F1, A2)
  * URLs in GitLab are not "eternally persistent"
  * GitLab projects cannot easily be cited
* Notice! Git is not equally suitable for all file formats

