
---

# Agenda

* 9:00 - 10:00 — Block 1 (Git Basics)
  * 09:00 - Welcome
  * 09:30 - Recap git
  * 09:45 - Shaun the Sheep
* 10:00 - 10:15 — *Break*
* 10:15 - 11:45 — Block 2 (Git in Practice)
  * 10:15 - Git Tac Toe
  * 11:15 - GitLab Walkthrough
* 11:45 - 12:30 — *Lunch Break*
* 12:30 - 14:00 — Block 3 (GitLab)
  * 12:30 - Issue Management
  * 12:35 - FAIR Principles with Git(Lab)
  * 12:55 - GitLab RDM Flow: Validate, Collaborate, Publish
  * 13:50 - Wrap Up, Feedback

---
class: sparse

# Introduction

## Who are we?
## Who are you?
* Introduce yourself in one sentence.
* Which operating system do you use?
* How experienced are you with Git?
  * 1: Not at all
  * 2: Used Git before
  * 3: Regular user

---
class: sparse

# Offline Preparation

- Communication (Links, Feedback) via Concept Board
- Please log in as Guest
- [Link to Concept Board]({{ workshop.links.concept_board_start }})

<span style="float:right;"> ![](img/tiny_cc_concept.png) </span>

---
class: sparse

# Online Preparation

* Did the [online preparation]({{ workshop.links.prep_project }}) work for everyone?
* Do you have git installed?
* Did you manage to log into [{{ workshop.links.gitlab_instance }}]({{ workshop.links.gitlab_instance }})?
* CLI basics?
