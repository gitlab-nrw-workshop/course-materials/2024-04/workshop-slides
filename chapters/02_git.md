
---
class: sparse

# Git: Remote Version Control

* What is the difference between Git and GitLab?
* How does Git work internally?
* What are the basic commands?

[Slides]({{ workshop.links.git_slides }})



---


# Git: Workflow

.center[
  ![Git Concepts](img/git_concepts.png)
]

---
class: wide

# External Resource:

.center[
  [![Explain git with D3](img/explaingitwithd3.png)](http://onlywei.github.io/explain-git-with-d3/)
]


---
class: sparse

# Which files work best with git?

Plain text files such as
* Source code
* Data sets, measurement files (CSV, TSV)
* Structured data files (XML, JSON)
* Markup text documents (TeX, Markdown, HTML)

---

# Basic git commands - *Global Configuration*

## Only necessary once per device:

Configure `<User Name>` as your global git user name
```bash
git config --global user.name <User Name>
# e.g.
git config --global user.name "Henning Timm"
```

Configure `<Email>` as global git mail address
```bash
git config --global user.email <Email>
# e.g.
git config --global user.email "henning.timm@uni-due.de"
```

---

# Basic git commands - *Interacting with git repos*

Initialize an empty git repository in the current folder
```bash
git init
```

Add files to the staging area
```bash
git add
# e.g.
git add .
```

Commit changes from the staging area to the repository
```bash
git commit -m <commit message>
# e.g.
git commit –m "First Commit"
```

---

Clone a remote repository
```bash
git clone <repository url>
# e.g.
git clone {{ workshop.links.gitlab_instance }}/grp/repo.git
```

Transfer version history to the remote repository
```bash
git push <repository url> <branch>
# e.g.
git push {{ workshop.links.gitlab_instance }}/grp/repo.git main
# push the current branch to the default remote
git push
```

Get the version history from the remote repository and merge it into the working copy.
```bash
git pull <repository url> <branch>
# e.g.
git pull {{ workshop.links.gitlab_instance }}/grp/repo.git main
# pull the current branch from the default remote
git pull
```

---
class: wide

# External Resource:

<p style="margin-top:-3em">
.center[
![Git Cheat Sheet by by Hylke Bons based on work by Zack Rusin and Sébastien Pierre. This work is licensed under the Creative Commons Attribution 3.0 License.](https://raw.githubusercontent.com/hbons/git-cheat-sheet/master/preview.png)
]


---
class: sparse

# Shaun the Sheep

* [Concept Board]({{ workshop.links.concept_board_shaun }})
* Further reading
  * Interactive tutorials for advanced concept, e.g. branching, merging: https://learngitbranching.js.org/
  * Git Purr — Git Commands Explained with Cats: https://girliemac.com/blog/2017/12/26/git-purr/
  * Git treasure hunt:<br>https://git.rwth-aachen.de/patrick.jueptner/git-hunt-lvl1-7


---
class: title-blue, img-only

# *Break (15 min)*

![](img/pause1.jpg)

Foto: Marco Verch | [ccnull.de](https://ccnull.de/foto/coffe-break-concept-with-cup-of-coffee-and-mobile-phone-on-the-table/1013743) | CC-BY 2.0
